def parse_int(request, param_name, default, min=float('-inf'), max=float('inf')):

    value = request.args.get(param_name, str(default))

    if not value.isdigit():
        raise ValueError("Error: int is expected for length param")

    value = int(value)

    if not (min < value < max):
        raise ValueError("Error: length is out of range")

    return value