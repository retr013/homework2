import datetime
import calc as c
from parsers import parse_int
from utils import generate_password, execute_query, format_list
import requests
from flask import Flask, request, Response
from faker import Faker


faker = Faker()

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello, World!!!"

@app.route('/now')
def now():
    return str(datetime.datetime.now())

@app.route('/random')
def get_random():
    try:
        length = parse_int(request, "length", 10, min=2, max=100)
    except ValueError as ex:
        return Response(str(ex), status = 400)
    return generate_password(length)

@app.route('/avr_data')
def get_avr_data():
    f = c.students()
    return f

@app.route('/requirements')
def get_requirements():
    with open('requirements.txt', 'r') as file:
        f = file.read()
        return f

@app.route('/customers')
def get_customers():
    query = 'select * from customers'
    result = execute_query(query)
    return format_list(result)

@app.route('/bitcoin_rate')
def get_bitcoin_rate(currency='USD'):
    response = requests.get('https://bitpay.com/api/rates')
    response.raise_for_status()
    json_response = response.json()
    currency = request.args.get('currency', 'USD')
    for dict in json_response:
        if dict["code"] == currency.upper():
            return str(dict['rate'])
        else:
            continue

@app.route('/random_users')
def gen_random_users():
    result = ''
    try:
        count = parse_int(request, "count", 100)
    except ValueError as ex:
        return Response(str(ex), status = 400)
    for i in range(1, count+1):
        result += str(i) + '. ' + faker.name() + '<br>'
    return result

@app.route('/unique_names')
def get_unique_names():
    sql_query = 'SELECT Firstname, LastName FROM Customers'
    result_sql = execute_query(sql_query)
    result = []
    for i in result_sql:
        result.append(i[0] + ' ' + i[1])
    result = set(result)
    return str(result)

@app.route('/tracks_count')
def get_tracks_count():
    sql_query = 'SELECT * from Tracks'
    result_sql = execute_query(sql_query)
    return str(len(result_sql))

@app.route('/customers_from')
def get_customers_from():
    city = "'" + request.args.get('city', "") + "'"
    country = "'" + request.args.get('country', "") + "'"
    sql_query = f"select * from Customers WHERE Country = {country} and City = {city}"
    result_sql = execute_query(sql_query)
    if len(result_sql) > 0:
        return str(result_sql)
    else:
        return 'Empty request'



app.run()
